package it.dstech.dao;

import it.dstech.model.Persona;

public interface PersonaDao {
	
	Persona search(Long id);

	boolean create(Persona persona);

	boolean remove(Persona persona);

	boolean update(Persona persona);
}
