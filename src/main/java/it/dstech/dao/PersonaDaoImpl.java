
package it.dstech.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import it.dstech.model.Persona;



@Component
@Transactional
public class PersonaDaoImpl implements PersonaDao {

  @Autowired
  private SessionFactory sessionFactory;
  
  @Override
  public Persona search(Long id) {
    
    return sessionFactory.getCurrentSession().find(Persona.class, id);
  }

  @Override
  public boolean create(Persona persona) {
    sessionFactory.getCurrentSession().persist(persona);
    sessionFactory.getCurrentSession().flush();
    return true;
  }

  @Override
  public boolean remove(Persona persona) {
    sessionFactory.getCurrentSession().delete(persona);
    return true;
  }

  @Override
  public boolean update(Persona persona) {
    sessionFactory.getCurrentSession().merge(persona);
    sessionFactory.getCurrentSession().flush();
    return true;
  }

}
