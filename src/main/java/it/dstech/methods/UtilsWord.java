package it.dstech.methods;

public class UtilsWord {

	private static final String VOCALI = "AEIOU";

	public static int numCons(String cognome) {
		int consonanti = 0;
		for (int i = 0; i < cognome.length(); i++) {
			if (!isVoc(cognome.charAt(i))) {
				consonanti++;
			}
		}
		return consonanti;
	}

	public static String primeCons(String cognome, int numero) {
		String consonanti = "";
		for (int i = 0; i < cognome.length(); i++) {
			if (!isVoc(cognome.charAt(i))) {
				if (consonanti.length() < numero) {
					consonanti += Character.toString(cognome.charAt(i));
				}
			}
		}
		return consonanti;
	}

	public static String consI(String string, int i) {
		int cont = 0;
		for (int j = 0; j < string.length(); j++) {
			if (!isVoc(string.charAt(j))) {
				cont++;
				if (cont == i) {
					return Character.toString(string.charAt(j));
				}
			}
		}
		return null;
	}

	public static String primeVoc(String string, int numero) {
		String vocali = "";
		for (int i = 0; i < string.length(); i++) {
			if (isVoc(string.charAt(i))) {
				if (vocali.length() < numero) {
					vocali += Character.toString(string.charAt(i));
				}
			}
		}
		return vocali;
	}

	public static String nXChar(int n) {
		String risultato = "";
		for (int i = 0; i < n; i++) {
			risultato += "X";
		}
		return risultato;
	}

	public static String removeSpaziVuoti(String string) {
		return string.replaceAll("\\s+", "");
	}

	public static boolean isVoc(char character) {
		return VOCALI.contains(Character.toString(character));
	}

	public static String stringPari(String string) {
		String risultato = "";
		for (int i = 0; i < string.length(); i++) {
			if ((i + 1) % 2 == 0) {
				risultato += Character.toString(string.charAt(i));
			}
		}
		return risultato;
	}

	public static String stringDispari(String string) {
		String risultato = "";
		for (int i = 0; i < string.length(); i++) {
			if ((i + 1) % 2 == 1) {
				risultato += Character.toString(string.charAt(i));
			}
		}
		return risultato;
	}
}
