package it.dstech.service;

import it.dstech.model.Persona;

public interface PersonaService {
	
	Persona search(Long id);

	boolean create(Persona persona);

	boolean remove(Persona persona);

	boolean update(Persona persona);
}
