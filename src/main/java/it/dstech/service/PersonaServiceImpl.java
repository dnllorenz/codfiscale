package it.dstech.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.dstech.dao.PersonaDao;
import it.dstech.methods.UtilsWord;
import it.dstech.model.Persona;

@Service
@Transactional
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	PersonaDao dao;

	@Override
	public Persona search(Long id) {
		return dao.search(id);
	}

	@Override
	public boolean create(Persona persona) {
		calcolaCodFisc();
		return dao.create(persona);
	}

	@Override
	public boolean remove(Persona persona) {
		return dao.remove(persona);
	}

	@Override
	public boolean update(Persona persona) {
		return dao.update(persona);
	}
	
	
	

	public String codCognome(String cognome) {
		{
			String codiceCognome;
			int numConsonanti;
			cognome = UtilsWord.removeSpaziVuoti(cognome).toUpperCase();

			if (cognome.length() >= 3) {

				numConsonanti = UtilsWord.numCons(cognome);

				if (numConsonanti >= 3) {

					codiceCognome = UtilsWord.primeCons(cognome, 3);
				} else {

					codiceCognome = UtilsWord.primeCons(cognome, numConsonanti);
					codiceCognome += UtilsWord.primeVoc(cognome, 3 - numConsonanti);
				}
			} else {

				int numeroCaratteri = cognome.length();
				codiceCognome = cognome + UtilsWord.nXChar(3 - numeroCaratteri);
			}

			return codiceCognome;
		}
	}

	public String codNome(String nome) {

		String codiceNome;
		int numeroConsonanti;
		nome = UtilsWord.removeSpaziVuoti(nome).toUpperCase();

		if (nome.length() >= 3) {

			numeroConsonanti = UtilsWord.numCons(nome);

			if (numeroConsonanti >= 4) {

				codiceNome = UtilsWord.consI(nome, 1) + UtilsWord.consI(nome, 3) + UtilsWord.consI(nome, 4);
			} else if (numeroConsonanti >= 3) {

				codiceNome = UtilsWord.primeCons(nome, 3);
			} else {

				codiceNome = UtilsWord.primeCons(nome, numeroConsonanti);
				codiceNome += UtilsWord.primeVoc(nome, 3 - numeroConsonanti);
			}
		} else {

			int numeroCaratteri = nome.length();
			codiceNome = nome + UtilsWord.nXChar(3 - numeroCaratteri);
		}

		return codiceNome;
	}

	public String calcolaeDataSesso(int anno, int mese, int giorno, String sesso) {
		String codDataNascitaESesso;
		String codAnno;
		String codMese;
		String codGiornoESesso;

		codAnno = codiceAnno(anno);
		codMese = codiceMese(mese);
		codGiornoESesso = codiceGiornoESesso(giorno, sesso);

		codDataNascitaESesso = codAnno + codMese + codGiornoESesso;

		return codDataNascitaESesso;
	}

	public String codiceAnno(int anno) {
		return Integer.toString(anno).substring(2);
	}

	public String codiceMese(int mese) {
		String risultato;
		mese++;
		switch (mese) {
		case 1:
			risultato = "A";
			break;
		case 2:
			risultato = "B";
			break;
		case 3:
			risultato = "C";
			break;
		case 4:
			risultato = "D";
			break;
		case 5:
			risultato = "E";
			break;
		case 6:
			risultato = "H";
			break;
		case 7:
			risultato = "L";
			break;
		case 8:
			risultato = "M";
			break;
		case 9:
			risultato = "P";
			break;
		case 10:
			risultato = "R";
			break;
		case 11:
			risultato = "S";
			break;
		case 12:
			risultato = "T";
			break;
		default:
			risultato = "";
			break;
		}
		return risultato;
	}

	public String codiceGiornoESesso(int giorno, String sesso) {
		String codiceGiorno = String.format("%02d", giorno);

		if (sesso.equals("F")) {
			int codiceGiornoIntero;
			codiceGiornoIntero = Integer.parseInt(codiceGiorno);
			codiceGiornoIntero += 40;
			codiceGiorno = Integer.toString(codiceGiornoIntero);
		}

		return codiceGiorno;
	}

	public String carattereDiControllo(String codice)  {

		String pari = UtilsWord.stringPari(codice);
		String dispari = UtilsWord.stringDispari(codice);

		int sommaDispari = convCharDisp(dispari);
		int sommaPari = convCharPari(pari);

		int somma = sommaDispari + sommaPari;
		int resto = (int) somma % 26;
		char restoConvertito = convResto(resto);

		return Character.toString(restoConvertito);
	}

	public int convCharDisp(String string) {
		int risultato = 0;
		for (int i = 0; i < string.length(); i++) {
			char carattere = string.charAt(i);
			switch (carattere) {
			case '0':
			case 'A':
				risultato += 1;
				break;
			case '1':
			case 'B':
				risultato += 0;
				break;
			case '2':
			case 'C':
				risultato += 5;
				break;
			case '3':
			case 'D':
				risultato += 7;
				break;
			case '4':
			case 'E':
				risultato += 9;
				break;
			case '5':
			case 'F':
				risultato += 13;
				break;
			case '6':
			case 'G':
				risultato += 15;
				break;
			case '7':
			case 'H':
				risultato += 17;
				break;
			case '8':
			case 'I':
				risultato += 19;
				break;
			case '9':
			case 'J':
				risultato += 21;
				break;
			case 'K':
				risultato += 2;
				break;
			case 'L':
				risultato += 4;
				break;
			case 'M':
				risultato += 18;
				break;
			case 'N':
				risultato += 20;
				break;
			case 'O':
				risultato += 11;
				break;
			case 'P':
				risultato += 3;
				break;
			case 'Q':
				risultato += 6;
				break;
			case 'R':
				risultato += 8;
				break;
			case 'S':
				risultato += 12;
				break;
			case 'T':
				risultato += 14;
				break;
			case 'U':
				risultato += 16;
				break;
			case 'V':
				risultato += 10;
				break;
			case 'W':
				risultato += 22;
				break;
			case 'X':
				risultato += 25;
				break;
			case 'Y':
				risultato += 24;
				break;
			case 'Z':
				risultato += 23;
				break;
			}
		}
		return risultato;
	}

	public int convCharPari(String string) {
		int risultato = 0;
		for (int i = 0; i < string.length(); i++) {
			char carattere = string.charAt(i);
			int numero = Character.getNumericValue(carattere);

			if (Character.isLetter(carattere)) {

				numero = carattere - 65;
				risultato += numero;
			} else {

				risultato += numero;
			}

		}
		return risultato;
	}

	public char convResto(int resto) {
		return (char) (resto + 65);
	}

	
	//vuoto 
	public String calcolaCodiceComune(String comune) {

	return comune;
	}
	
	public String calcolaCodFisc()  {

		Persona persona = new Persona();

		String codiceCognome = codCognome(persona.getCognome());
		String codiceNome = codNome(persona.getNome());
		String codiceDataNascitaESesso = calcolaeDataSesso(persona.getAnno(), persona.getMese(), persona.getGiorno(),
				persona.getSesso());
		String codiceComunale = calcolaCodiceComune(persona.getComuneDiNascita());

		String risultato = codiceCognome + codiceNome + codiceDataNascitaESesso+codiceComunale;

		String carattereDiControllo = carattereDiControllo(risultato);

		risultato += carattereDiControllo;

		return risultato;

	}

}
