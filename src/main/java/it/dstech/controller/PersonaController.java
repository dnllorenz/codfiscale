package it.dstech.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.dstech.model.Persona;
import it.dstech.service.PersonaService;

@RestController
public class PersonaController {
	@Autowired
	PersonaService persona;

	@RequestMapping(value = "/persona/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Persona ricercaPersona(@PathVariable(value = "id") Long id) {
		return persona.search(id);
	}

	@RequestMapping(value = "/personaAdd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean aggiungiPersona(@RequestBody Persona p) {
		return persona.create(p);
	}

	@RequestMapping(value = "/persona", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean modificaPersona(@RequestBody Persona p) {
		return persona.update(p);
	}

	@RequestMapping(value = "/persona", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean rimuoviPersona(@RequestBody Persona p) {
		return persona.remove(p);
	}

}
